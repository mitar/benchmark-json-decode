## Benchmark comparing JSON decoding in node and Go

### Running

```
$ make
$ make generate
$ ./run.sh
```

### Results

Results using Go v1.16 (and dependencies from [go.mod](./go.mod)) and node v16.0.0. For Go, there is a version with known `struct` and version with a `map[string]interface{}` interface. Moreover, we use [standard `encoding/json`](https://golang.org/pkg/encoding/json/) and [`jsoniter`](https://github.com/json-iterator/go).
Measured on a range of JSON files from [10 regular and 10 array fields](./10.json) (~23 KB) to [200 regular and 200 array fields](./200.json) (~1.3 MB).
Each run repeated 10 times and averaged.
See [raw measurements](./raw-measurements.txt).

|  | node | go interface std | go interface jsoniter | go struct std | go struct jsoniter |
|--|--|--|--|--|--|
| 10 |0.00008 | 0.00016 | 0.00011 | 0.00013 | 0.00060 |
| 20 |0.00016 | 0.00036 | 0.00022 | 0.00025 | 0.00063 |
| 30 |0.00022 | 0.00058 | 0.00035 | 0.00040 | 0.00067 |
| 40 |0.00042 | 0.00088 | 0.00055 | 0.00057 | 0.00072 |
| 50 |0.00058 | 0.00119 | 0.00075 | 0.00079 | 0.00080 |
| 60 |0.00066 | 0.00154 | 0.00104 | 0.00101 | 0.00088 |
| 70 |0.00059 | 0.00200 | 0.00135 | 0.00129 | 0.00092 |
| 80 |0.00073 | 0.00239 | 0.00167 | 0.00158 | 0.00100 |
| 90 |0.00100 | 0.00289 | 0.00205 | 0.00189 | 0.00111 |
| 100 |0.00193 | 0.00333 | 0.00243 | 0.00224 | 0.00120 |
| 110 |0.00324 | 0.00388 | 0.00282 | 0.00262 | 0.00133 |
| 120 |0.00312 | 0.00445 | 0.00323 | 0.00300 | 0.00143 |
| 130 |0.00356 | 0.00563 | 0.00403 | 0.00351 | 0.00153 |
| 140 |0.00349 | 0.00594 | 0.00449 | 0.00388 | 0.00169 |
| 150 |0.00366 | 0.00673 | 0.00576 | 0.00432 | 0.00183 |
| 160 |0.00411 | 0.00779 | 0.00665 | 0.00483 | 0.00200 |
| 170 |0.00411 | 0.00906 | 0.00708 | 0.00540 | 0.00217 |
| 180 |0.00438 | 0.01020 | 0.00787 | 0.00597 | 0.00227 |
| 190 |0.00493 | 0.01057 | 0.00832 | 0.00652 | 0.00245 |
| 200 |0.00528 | 0.01143 | 0.00905 | 0.00711 | 0.00267 |

Parsing JSON in node is really fast and beats Go when no structure
about JSON is provided (which is how parsing in node is done). Only
with information about the structure, the non-standard `jsoniter` library
is faster.