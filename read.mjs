import fs from 'fs';
import { performance } from 'perf_hooks';

const json = fs.readFileSync(process.argv[2]);

const before = performance.now();
const obj = JSON.parse(json);
const after = performance.now();

console.log((after - before) / 1000);
