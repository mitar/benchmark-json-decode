#!/bin/bash

for i in $(seq 10 10 200); do
  echo node "$i"
  for r in $(seq 1 1 10); do
    node read.mjs "$i.json"
  done
done

for p in interface struct ; do
  for t in std jsoniter ; do
    for i in $(seq 10 10 200); do
      echo go "$p" "$t" "$i"
      for r in $(seq 1 1 10); do
        "./read-$p" "$t" "$i.json"
      done
    done
  done
done
