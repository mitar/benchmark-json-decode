#!/usr/bin/python3

import collections

with open('raw-measurements.txt', 'r', encoding='utf8') as file:
  lines = file.readlines()

header = []
measurements = collections.defaultdict(list)

for i in range(0, int(len(lines) / 11)):
  chunk = lines[i*11:(i+1)*11]
  title, size = chunk[0].strip().rsplit(' ', 1)
  size = int(size)
  avg = sum(float(c) for c in chunk[1:]) / 10

  if title not in header:
    header.append(title)
  measurements[size].append(avg)

print("|  | " + " | ".join(header) + " |")
print("|--|-" + ("-|-" * (len(header) - 1)) + "-|")

for size in sorted(measurements.keys()):
  numbers = ["%.5f" % n for n in measurements[size]]
  print(f"| {size} |" + " | ".join(numbers) + " |")
