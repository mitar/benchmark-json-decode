import crypto from 'crypto';

const FIELDS = parseInt(process.argv[2] || 100);
const ARRAYS = parseInt(process.argv[3] || 100);
const VALUES_PER_ARRAY = parseInt(process.argv[4] || 100);

const obj = {};

for (let i = 0; i < FIELDS; i++) {
  obj[`field${i}`] = crypto.randomBytes(1000).toString('hex');
}
for (let i = 0; i < ARRAYS; i++) {
  const arr = [];
  for (let j = 0; j < VALUES_PER_ARRAY; j++) {
    arr.push(crypto.randomBytes(10).toString('hex'));
  }
  obj[`array${i}`] = arr;
}

console.log(JSON.stringify(obj));

