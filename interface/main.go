package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	jsoniter "github.com/json-iterator/go"
)

func main() {
	data, err := ioutil.ReadFile(os.Args[2])
	if err != nil {
		log.Fatalln(err)
	}

	var obj map[string]interface{}
	var before time.Time
	var after time.Time
	switch os.Args[1] {
	case "std":
		before = time.Now()
		err = json.Unmarshal(data, &obj)
		after = time.Now()
	case "jsoniter":
		before = time.Now()
		err = jsoniter.Unmarshal(data, &obj)
		after = time.Now()
	}
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Printf("%f\n", after.Sub(before).Seconds())
}
