SHELL := /bin/bash

.PHONY: clean

go_binaries = read-interface read-struct

build: $(go_binaries)

read-interface: interface/*.go
	go build -o $@ gitlab.com/mitar/benchmark-json-decode/interface

read-struct: struct/*.go
	go build -o $@ gitlab.com/mitar/benchmark-json-decode/struct

generate:
	./generate.sh

clean:
	rm -f $(go_binaries) *.json
